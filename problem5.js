const problem4 = require("./problem4");


function problem5(inventoryData){

    const years=problem4(inventoryData)
    
    const oldcars=[]
    for(let i=0;i<years.length;i++){

        if(years[i]<2000){
            oldcars.push(inventoryData[i])
            
        }

    }
    
    return oldcars

}

module.exports = problem5;